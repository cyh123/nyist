package com.web.nyist.repository;

import com.web.nyist.model.AdminMenuButton;
import com.web.nyist.model.AdminRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by penghuiping on 1/20/15.
 */
@Repository
public interface AdminMenuButtonRepository extends BaseRepository<AdminMenuButton,Long> {

    @Query("from AdminMenuButton a where a.parent is null")
    List<AdminMenuButton> findRootMenus();

    @Query("FROM AdminMenuButton a where a.parent=:parent")
    List<AdminMenuButton> findMenusByParent(@Param("parent") AdminMenuButton parent);

    @Query("select b from AdminRole a join a.adminMenuButtons b with a=:role")
    List<AdminMenuButton> findMenusByRole(@Param("role")AdminRole role);

    @Query("from AdminMenuButton a where a.parent is null and a.enable=1")
    List<AdminMenuButton> findRootMenusEnabled();

    @Query("FROM AdminMenuButton a where a.parent=:parent and a.enable=1")
    List<AdminMenuButton> findMenusEnabledByParent(@Param("parent") AdminMenuButton parent);

    @Query("select b from AdminRole a join a.adminMenuButtons b with a=:role and a.enable=1")
    List<AdminMenuButton> findMenusEnabledByRole(@Param("role") AdminRole role);
}
