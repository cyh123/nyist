package com.web.nyist.repository;

import com.web.nyist.model.AdminRole;
import org.springframework.stereotype.Repository;

/**
 * Created by penghuiping on 1/20/15.
 */
@Repository
public interface AdminRoleRepository extends BaseRepository<AdminRole,Long>{

}
