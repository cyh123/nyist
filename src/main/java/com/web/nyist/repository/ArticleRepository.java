package com.web.nyist.repository;

import com.web.nyist.model.Articles;
import org.springframework.stereotype.Repository;

/**
 * Created by penghuiping on 1/19/15.
 */
@Repository
public interface ArticleRepository extends BaseRepository<Articles,Long> {
}
