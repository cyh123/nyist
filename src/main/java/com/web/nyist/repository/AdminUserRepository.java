package com.web.nyist.repository;

import com.web.nyist.model.AdminUser;
import org.springframework.stereotype.Repository;

/**
 * Created by penghuiping on 1/19/15.
 */
@Repository
public interface AdminUserRepository extends BaseRepository<AdminUser,Long> {
    AdminUser findByUsernameAndPassword(String username, String password);
}
