package com.web.nyist.repository.impl;

import com.web.nyist.model.AdminUser;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by penghuiping on 2/20/15.
 */
public class UserRepositoryImpl {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public AdminUser findByUsernameAndPassword(String username, String password) {
        Query query = entityManager.createQuery("select a from AdminUser a where a.username=?1 and a.password=?2 and a.enable=1");
        query.setParameter(1, username);
        query.setParameter(2, password);
        List<AdminUser> rs = query.setFirstResult(0).setMaxResults(1).getResultList();
        if (rs.size() > 0) {
            AdminUser user = rs.get(0);
            return user;
        } else {
            return null;
        }
    }
}
