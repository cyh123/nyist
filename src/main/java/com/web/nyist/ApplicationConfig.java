package com.web.nyist;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author CaoYongHao.
 * @date 2017-12-01
 */
@Configuration
@ImportResource(locations={"classpath:applicationContext.xml"})
public class ApplicationConfig {


}
