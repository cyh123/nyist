package com.web.nyist.service.db.impl;

import com.web.nyist.dto.admin.DataGridPageDto;
import com.web.nyist.service.db.BaseService;
import org.springframework.data.domain.Page;

/**
 * Created by penghuiping on 16/8/12.
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {

    protected <T> DataGridPageDto<T> toDataGridPageDto(Page<T> page) {
        DataGridPageDto<T> dataGridPageDto = new DataGridPageDto<T>();
        dataGridPageDto.setRows(page.getContent());
        dataGridPageDto.setTotal(page.getTotalElements());
        return dataGridPageDto;
    }
}
