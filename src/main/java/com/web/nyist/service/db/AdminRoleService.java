package com.web.nyist.service.db;


import com.web.nyist.model.AdminRole;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
public interface AdminRoleService extends BaseService<AdminRole> {

    List<AdminRole> findAllEnabled();
}
