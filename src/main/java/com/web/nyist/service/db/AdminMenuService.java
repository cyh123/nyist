package com.web.nyist.service.db;

import com.web.nyist.model.AdminMenuButton;
import com.web.nyist.model.AdminRole;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
public interface AdminMenuService extends BaseService<AdminMenuButton> {

    List<AdminMenuButton> findMenusEnabledByRole(AdminRole adminRole);

    List<AdminMenuButton> findRootMenus();

    List<AdminMenuButton> findMenusByParent(AdminMenuButton parent);

    List<AdminMenuButton> findMenusByRole(AdminRole role);

    List<AdminMenuButton> findRootMenusEnabled();

    List<AdminMenuButton> findMenusEnabledByParent(AdminMenuButton parent);

}
