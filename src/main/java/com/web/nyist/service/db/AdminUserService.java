package com.web.nyist.service.db;

import com.web.nyist.model.AdminUser;

/**
 * Created by penghuiping on 16/8/12.
 */
public interface AdminUserService extends BaseService<AdminUser> {

    AdminUser findByUsernameAndPassword(String username, String password);
}
