package com.web.nyist.service.db.impl;

import com.web.nyist.dto.admin.DataGridPageDto;
import com.web.nyist.model.AdminUser;
import com.web.nyist.repository.AdminUserRepository;
import com.web.nyist.service.db.AdminUserService;
import com.web.nyist.specification.BaseSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
@Service(value = "adminUserService")
public class AdminUserServiceImpl extends BaseServiceImpl<AdminUser> implements AdminUserService {

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Override
    public AdminUser findOne(Long id) {
        return adminUserRepository.findOne(id);
    }

    @Override
    public AdminUser save(AdminUser obj) {
        return adminUserRepository.save(obj);
    }

    @Override
    public void delete(AdminUser obj) {
        adminUserRepository.delete(obj);
    }

    @Override
    public void delete(List<AdminUser> objs) {
        adminUserRepository.delete(objs);
    }

    @Override
    public List<AdminUser> findAll(Iterable<Long> ids) {
        return (List<AdminUser>) adminUserRepository.findAll(ids);
    }

    @Override
    public List<AdminUser> findAll() {
        return (List<AdminUser>) adminUserRepository.findAll();
    }

    @Override
    public DataGridPageDto<AdminUser> query(Integer pageNum, Integer pageSize, String searchParams) {
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize, Sort.Direction.DESC, "id");
        Page<AdminUser> userPage = adminUserRepository.findAll(BaseSpecs.<AdminUser>getSpecs(searchParams), pageRequest);
        PageImpl<AdminUser> adminUserPage = new PageImpl<AdminUser>(userPage.getContent(), null, userPage.getTotalElements());
        return toDataGridPageDto(adminUserPage);
    }

    @Override
    public AdminUser findByUsernameAndPassword(String username, String password) {
        return adminUserRepository.findByUsernameAndPassword(username, password);
    }
}
