package com.web.nyist.service.db.impl;

import com.web.nyist.dto.admin.DataGridPageDto;
import com.web.nyist.model.AdminRole;
import com.web.nyist.repository.AdminRoleRepository;
import com.web.nyist.service.db.AdminRoleService;
import com.web.nyist.specification.BaseSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
@Service(value = "adminRoleService")
public class AdminRoleServiceImpl extends BaseServiceImpl<AdminRole> implements AdminRoleService {
    @Autowired
    private AdminRoleRepository adminRoleRepository;

    @Override
    public AdminRole findOne(Long id) {
        return adminRoleRepository.findOne(id);
    }

    @Override
    public AdminRole save(AdminRole obj) {
        return adminRoleRepository.save(obj);
    }

    @Override
    public void delete(AdminRole obj) {
        adminRoleRepository.delete(obj);
    }

    @Override
    public void delete(List<AdminRole> objs) {
        adminRoleRepository.delete(objs);
    }

    @Override
    public List<AdminRole> findAll(Iterable<Long> ids) {
        return (List<AdminRole>) adminRoleRepository.findAll(ids);
    }

    @Override
    public List<AdminRole> findAllEnabled() {
        return (List<AdminRole>) adminRoleRepository.findAllEnabled();
    }

    @Override
    public List<AdminRole> findAll() {
        return (List<AdminRole>) adminRoleRepository.findAll();
    }

    @Override
    public DataGridPageDto<AdminRole> query(Integer pageNum, Integer pageSize, String searchParams) {
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize, Sort.Direction.DESC, "id");
        Page<AdminRole> userPage = adminRoleRepository.findAll(BaseSpecs.<AdminRole>getSpecs(searchParams), pageRequest);
        PageImpl<AdminRole> adminRolePage = new PageImpl<AdminRole>(userPage.getContent(), null, userPage.getTotalElements());
        return toDataGridPageDto(adminRolePage);
    }
}
