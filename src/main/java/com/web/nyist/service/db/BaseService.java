package com.web.nyist.service.db;

import com.web.nyist.dto.admin.DataGridPageDto;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
public interface BaseService<T> {
    T findOne(Long id);

    T save(T obj);

    void delete(T obj);

    void delete(List<T> objs);

    List<T> findAll(Iterable<Long> ids);

    List<T> findAll();

    DataGridPageDto<T> query(Integer pageNum, Integer pageSize, String searchParams);
}
