package com.web.nyist.service.db.impl;

import com.web.nyist.dto.admin.DataGridPageDto;
import com.web.nyist.model.AdminMenuButton;
import com.web.nyist.model.AdminRole;
import com.web.nyist.repository.AdminMenuButtonRepository;
import com.web.nyist.service.db.AdminMenuService;
import com.web.nyist.specification.BaseSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
@Service(value = "adminMenuService")
public class AdminMenuServiceImpl extends BaseServiceImpl<AdminMenuButton> implements AdminMenuService {

    @Autowired
    private AdminMenuButtonRepository adminMenuButtonRepository;

    @Override
    public AdminMenuButton findOne(Long id) {
        return adminMenuButtonRepository.findOne(id);
    }

    @Override
    public AdminMenuButton save(AdminMenuButton obj) {
        return adminMenuButtonRepository.save(obj);
    }

    @Override
    public void delete(AdminMenuButton obj) {
        adminMenuButtonRepository.delete(obj);
    }

    @Override
    public void delete(List<AdminMenuButton> objs) {
        adminMenuButtonRepository.delete(objs);
    }

    @Override
    public List<AdminMenuButton> findAll(Iterable<Long> ids) {
        return (List<AdminMenuButton>) adminMenuButtonRepository.findAll(ids);
    }

    @Override
    public List<AdminMenuButton> findAll() {
        return (List<AdminMenuButton>) adminMenuButtonRepository.findAll();
    }

    @Override
    public DataGridPageDto<AdminMenuButton> query(Integer pageNum, Integer pageSize, String searchParams) {
        PageRequest pageRequest = new PageRequest(pageNum - 1, pageSize, Sort.Direction.DESC, "id");
        Page<AdminMenuButton> userPage = adminMenuButtonRepository.findAll(BaseSpecs.<AdminMenuButton>getSpecs(searchParams), pageRequest);
        PageImpl<AdminMenuButton> adminUserPage = new PageImpl<AdminMenuButton>(userPage.getContent(), null, userPage.getTotalElements());
        return toDataGridPageDto(adminUserPage);
    }

    @Override
    public List<AdminMenuButton> findMenusEnabledByRole(AdminRole adminRole) {
        return adminMenuButtonRepository.findMenusEnabledByRole(adminRole);
    }

    @Override
    public List<AdminMenuButton> findRootMenus() {
        return adminMenuButtonRepository.findRootMenus();
    }

    @Override
    public List<AdminMenuButton> findMenusByParent(AdminMenuButton parent) {
        return adminMenuButtonRepository.findMenusByParent(parent);
    }

    @Override
    public List<AdminMenuButton> findMenusByRole(AdminRole role) {
        return adminMenuButtonRepository.findMenusByRole(role);
    }

    @Override
    public List<AdminMenuButton> findRootMenusEnabled() {
        return adminMenuButtonRepository.findRootMenusEnabled();
    }

    @Override
    public List<AdminMenuButton> findMenusEnabledByParent(AdminMenuButton parent) {
        return adminMenuButtonRepository.findMenusEnabledByParent(parent);
    }
}
