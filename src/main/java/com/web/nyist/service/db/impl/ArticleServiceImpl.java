package com.web.nyist.service.db.impl;

import com.web.nyist.dto.admin.DataGridPageDto;
import com.web.nyist.model.Articles;
import com.web.nyist.repository.ArticleRepository;
import com.web.nyist.service.db.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by penghuiping on 16/8/12.
 */
@Service
public class ArticleServiceImpl extends BaseServiceImpl<Articles> implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;


    @Override
    public Articles findOne(Long id) {
        return articleRepository.findOne(id);
    }

    @Override
    public Articles save(Articles obj) {
        return articleRepository.save(obj);
    }

    @Override
    public void delete(Articles obj) {
        articleRepository.delete(obj);
    }

    @Override
    public void delete(List<Articles> objs) {
        articleRepository.delete(objs);
    }

    @Override
    public List<Articles> findAll(Iterable<Long> ids) {
        return (List<Articles>)articleRepository.findAll(ids);
    }

    @Override
    public List<Articles> findAll() {
        return (List<Articles>)articleRepository.findAll();
    }

    @Override
    public DataGridPageDto<Articles> query(Integer pageNum, Integer pageSize, String searchParams) {
        return null;
    }
}
