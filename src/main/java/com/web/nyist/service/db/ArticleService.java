package com.web.nyist.service.db;

import com.web.nyist.model.Articles;

/**
 * Created by penghuiping on 16/8/12.
 */
public interface ArticleService extends BaseService<Articles> {

}
