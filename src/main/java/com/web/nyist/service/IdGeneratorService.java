package com.web.nyist.service;

import com.web.nyist.common.Constant;
import com.web.nyist.util.RandomUtil;
import com.web.nyist.util.TimeUtil;
import org.springframework.stereotype.Service;

/**
 * Created by penghuiping on 5/15/15.
 */
@Service
public class IdGeneratorService {
    /**
     * 产生随机的登入token
     *
     * @return
     */
    public String generateLoginToken() {
        return Constant.RANDOM.ACCESS_TOKEN_PREFIX + TimeUtil.getNewTime() + RandomUtil.getRandomNumbers(6);
    }

    /**
     * 产生随机的imageId
     *
     * @return
     */
    public String generateImageId() {
        return null;
//        return new String(DigestUtil.bytes2hex(DigestUtil.MD5(Constant.RANDOM.IMAGE_ID_PREFIX + TimeUtil.getNewTime() + RandomUtil.getRandomNumbers(6))));
    }
}
