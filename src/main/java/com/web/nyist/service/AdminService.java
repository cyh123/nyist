package com.web.nyist.service;

import com.web.nyist.dto.admin.AdminMenuButtonDto;
import com.web.nyist.dto.admin.AdminUserDto;
import com.web.nyist.model.AdminMenuButton;
import com.web.nyist.model.AdminUser;
import com.web.nyist.service.db.AdminMenuService;
import com.web.nyist.service.db.AdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penghuiping on 11/12/15.
 *
 * 管理后台的一些通用service方法
 *
 */
@Service(value = "adminService")
public class AdminService {

    @Autowired
    AdminMenuService adminMenuService;

    @Autowired
    AdminUserService adminUserService;


    /**
     * 获取所有的有效的菜单信息
     * @return 菜单列表
     */
    public List<AdminMenuButtonDto> findTreeMenusEnabled() {
        List<AdminMenuButton> rootMenus = adminMenuService.findRootMenusEnabled();
        List<AdminMenuButtonDto> rootMenuDtos = new ArrayList<AdminMenuButtonDto>();
        for (AdminMenuButton menu : rootMenus) {
            //第一层，子菜单
            List<AdminMenuButton> children = adminMenuService.findMenusEnabledByParent(menu);
            List<AdminMenuButtonDto> menuButtonDtos = new ArrayList<AdminMenuButtonDto>();
            for (AdminMenuButton subMenu : children) {
                //第二次，按钮
                List<AdminMenuButton> subChildren = adminMenuService.findMenusEnabledByParent(subMenu);
                List<AdminMenuButtonDto> subChildrenDto = new ArrayList<AdminMenuButtonDto>();
                for (AdminMenuButton button : subChildren) {
                    subChildrenDto.add(new AdminMenuButtonDto(button));
                }
                menuButtonDtos.add(new AdminMenuButtonDto(subMenu, subChildrenDto));
            }
            rootMenuDtos.add(new AdminMenuButtonDto(menu, menuButtonDtos));
        }
        return rootMenuDtos;
    }

    /**
     * 获取所有的菜单信息
     *
     * @return 菜单列表
     */
    public List<AdminMenuButtonDto> findTreeMenus() {
        List<AdminMenuButton> rootMenus = adminMenuService.findRootMenus();
        List<AdminMenuButtonDto> rootMenuDtos = new ArrayList<AdminMenuButtonDto>();
        for (AdminMenuButton menu : rootMenus) {
            //第一层，子菜单
            List<AdminMenuButton> children = adminMenuService.findMenusByParent(menu);
            List<AdminMenuButtonDto> menuButtonDtos = new ArrayList<AdminMenuButtonDto>();
            for (AdminMenuButton subMenu : children) {
                //第二次，按钮
                List<AdminMenuButton> subChildren = adminMenuService.findMenusByParent(subMenu);
                List<AdminMenuButtonDto> subChildrenDto = new ArrayList<AdminMenuButtonDto>();
                for (AdminMenuButton button : subChildren) {
                    subChildrenDto.add(new AdminMenuButtonDto(button));
                }
                menuButtonDtos.add(new AdminMenuButtonDto(subMenu, subChildrenDto));
            }
            rootMenuDtos.add(new AdminMenuButtonDto(menu, menuButtonDtos));
        }
        return rootMenuDtos;
    }


    /**
     * 登入操作，用户名与密码正确，返回user对象，用户名与密码错误的话，返回null
     * @param username
     * @param password
     * @return 用户对象
     */
    public AdminUserDto login(String username, String password) {
        AdminUser user = adminUserService.findByUsernameAndPassword(username, password);
        AdminUserDto userDto = null;
        if (null != user) {
            //设置此用户的展示目录
            List<AdminMenuButtonDto> rootMenuDtos = findTreeMenusEnabled();
            List<AdminMenuButton> showMenus = adminMenuService.findMenusEnabledByRole(user.getRole());

            for (AdminMenuButtonDto menu : rootMenuDtos) {
                traverseTreeSetShowStatus(menu, showMenus);
            }

            userDto = new AdminUserDto(user, rootMenuDtos);
        }
        return userDto;
    }

    /**
     * 遍历树状结构,设置菜单与按钮是否显示
     *
     * @param adminMenuButtonDto
     */
    private void traverseTreeSetShowStatus(AdminMenuButtonDto adminMenuButtonDto, List<AdminMenuButton> showMenus) {
        if (menuAndButtonIsShow(adminMenuButtonDto, showMenus)) {
            adminMenuButtonDto.setIsShow(true);
        } else {
            adminMenuButtonDto.setIsShow(false);
        }

        if (!adminMenuButtonDto.getIsLeaf()) {
            List<AdminMenuButtonDto> children = adminMenuButtonDto.getChildren();
            for (AdminMenuButtonDto child : children) {
                traverseTreeSetShowStatus(child, showMenus);
            }
        }
    }

    /**
     * 遍历树状结构，查出对应的id对象
     *
     * @param adminMenuButtonDto
     * @param id
     * @return
     */
    private AdminMenuButtonDto traverseMenuAndButtonTreeFindOneById(AdminMenuButtonDto adminMenuButtonDto, Long id) {
        if (adminMenuButtonDto.getIsLeaf()) {
            if (id == adminMenuButtonDto.getId()) {
                return adminMenuButtonDto;
            } else {
                return null;
            }
        } else {
            if (id == adminMenuButtonDto.getId()) {
                return adminMenuButtonDto;
            }
            AdminMenuButtonDto temp = null;
            List<AdminMenuButtonDto> children = adminMenuButtonDto.getChildren();
            for (AdminMenuButtonDto child : children) {
                temp = traverseMenuAndButtonTreeFindOneById(child, id);
                if (null != temp) {
                    break;
                }
            }
            return temp;
        }
    }

    /**
     * 判断是否菜单与按钮需要显示
     *
     * @param adminMenuButtonDto
     * @param showMenus
     * @return
     */
    private Boolean menuAndButtonIsShow(AdminMenuButtonDto adminMenuButtonDto, List<AdminMenuButton> showMenus) {
        for (AdminMenuButton adminMenuButton : showMenus) {
            if (adminMenuButton.getId().equals(adminMenuButtonDto.getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据id找出对应的对象
     *
     * @param adminMenuButtonDtos
     * @param id
     * @return
     */
    public AdminMenuButtonDto findOneMenuAndButtonById(List<AdminMenuButtonDto> adminMenuButtonDtos, Long id) {
        AdminMenuButtonDto result = null;
        for (AdminMenuButtonDto adminMenuButtonDto : adminMenuButtonDtos) {
            result = traverseMenuAndButtonTreeFindOneById(adminMenuButtonDto, id);
            if (null != result) {
                break;
            }
        }
        return result;
    }



}
