package com.web.nyist.util;

/**
 * Created by penghuiping on 11/17/15.
 */
public class ThreadLocalUtil {
    private static final ThreadLocal<Object> threadLocal = new ThreadLocal<Object>();

    public static void set(Object object) {
        threadLocal.set(object);
    }

    public static Object get() {
        return threadLocal.get();
    }
}
