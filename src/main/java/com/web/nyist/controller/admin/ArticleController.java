package com.web.nyist.controller.admin;

import com.web.nyist.dto.JSONResponse;
import com.web.nyist.model.Articles;
import com.web.nyist.service.AdminService;
import com.web.nyist.service.db.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by penghuiping on 1/16/15.
 */
@Controller
@RequestMapping(ArticleController.BASE_URL)
public class ArticleController extends AdminController {
    public static final String BASE_URL = "/article/";

    @Autowired
    private ArticleService articleService;

    @Autowired
    AdminService adminService;

    @Resource(name = "mailSender")
    private JavaMailSender mailSender;


    @RequestMapping(value = "index")
    public ModelAndView login(ModelAndView modelAndView) throws Throwable {


        List<Articles> articleList=articleService.findAll();
        modelAndView.addObject("list",articleList);
        modelAndView.setViewName(BASE_URL+"/app.html");
        return modelAndView;
    }


    @RequestMapping(value = "submit")
    public ModelAndView submit(ModelAndView modelAndView,Articles articles) throws Throwable {

        articles.setCreateTime(new Date());
        articles.setUpdateTime(new Date());
        articles.setType(1);
        articles.setEnable(1);
        articles.setUserId(1L);
        articleService.save(articles);

        modelAndView.setViewName("redirect:/home"+"/blog");
        return modelAndView;
    }

    @RequestMapping(value = "detail")
    public ModelAndView detail(ModelAndView modelAndView,Long id) throws Throwable {


        Articles article=articleService.findOne(id);
        modelAndView.addObject("article",article);
        modelAndView.setViewName(BASE_URL+"/detail.html");
        return modelAndView;
    }

    @RequestMapping(value = "addArticle")
    public ModelAndView addArticle(ModelAndView modelAndView) throws Throwable {
        modelAndView.addObject("site","5");
        modelAndView.setViewName(BASE_URL+"/addArticle.html");
        return modelAndView;
    }



    @RequestMapping(value = "main")
    public ModelAndView main(ModelAndView modelAndView) throws Throwable {


        SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd");
        Long day=(new Date().getTime()-format.parse("20160601").getTime())/(24*60*60)/1000;

        modelAndView.addObject("site","0");
        modelAndView.addObject("day",day);

        modelAndView.setViewName(BASE_URL+"/home.html");
        return modelAndView;
    }

    @RequestMapping(value = "services")
    public ModelAndView services(ModelAndView modelAndView) throws Throwable {
        modelAndView.addObject("site","1");
        modelAndView.setViewName(BASE_URL+"/services.html");
        return modelAndView;
    }

//    @RequestMapping(value = "products")
//    public ModelAndView products(ModelAndView modelAndView) throws Throwable {
//        modelAndView.addObject("site","2");
//        modelAndView.setViewName(BASE_URL+"/products.html");
//        return modelAndView;
//    }

    @RequestMapping(value = "blog")
    public ModelAndView blog(ModelAndView modelAndView) throws Throwable {
        modelAndView.addObject("site","2");
        modelAndView.setViewName(BASE_URL+"/blog.html");
        return modelAndView;
    }

    @RequestMapping(value = "about")
    public ModelAndView about(ModelAndView modelAndView) throws Throwable {
        modelAndView.addObject("site","3");
        modelAndView.setViewName(BASE_URL+"/about.html");
        return modelAndView;
    }

    @RequestMapping(value = "contact")
    public ModelAndView contact(ModelAndView modelAndView) throws Throwable {
        modelAndView.addObject("site","4");
        modelAndView.setViewName(BASE_URL+"/contact.html");
        return modelAndView;
    }

    @RequestMapping(value = "bug")
    @ResponseBody
    public JSONResponse bug(String txt) throws Throwable {
        JSONResponse response=new JSONResponse();
        sendMail(txt,"534729859@qq.com",txt);
        return response;

    }

    /**
     * 方法名: sendMail
     * 描述: 发送邮件
     * 参数: @param subject
     * 参数: @param toMail
     * 参数: @param content
     * 参数: @param files
     * 参数: @return
     * 返回类型: boolean
     */
    public boolean sendMail(String subject, String toMail, String content) {
        boolean isFlag = false;
        Map<String, Object> logMap=new HashMap<String, Object>();
        try {
            MimeMessage message = mailSender.createMimeMessage();
            logMap.put("sendMailLog", "sendMailLog");
            String emails[] = toMail.split(",");

            logMap.put("emails", emails);

            for (int i = 0; i < emails.length; i++) {
                String email = emails[i];
                MimeMessageHelper helper = new MimeMessageHelper(message, true);
                logMap.put("sendInfoTo" + i, "发送给" + email + "mailInfo:" + "subject:" + subject + ";content:" + content + ";");
                helper.setFrom(new InternetAddress(((JavaMailSenderImpl) mailSender).getUsername(), "bug通知：" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + ""));
                helper.setSubject(subject);
                helper.setTo(email);
                helper.setText(content);
                isFlag = true;
                mailSender.send(message);
            }
        } catch (Exception e) {
            isFlag = false;
        }
//        logger.info("mail:" + logMap);
        return isFlag;
    }

}
