package com.web.nyist.controller.admin;

import com.web.nyist.common.Constant;
import com.web.nyist.dto.admin.AdminUserDto;
import com.web.nyist.model.AdminUser;
import com.web.nyist.repository.AdminUserRepository;
import com.web.nyist.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * Created by penghuiping on 1/16/15.
 */
@Controller
@RequestMapping(CommonController.BASE_URL)
public class CommonController extends AdminController {
    public static final String BASE_URL = "/admin/common/";

    @Autowired
    AdminUserRepository userRepository;

    @Autowired
    AdminService adminService;


    @RequestMapping(value = "login.do")
    public ModelAndView login(ModelAndView modelAndView) throws Throwable {
        modelAndView.setViewName("admin/common/login.htm");
        return modelAndView;
    }

    @RequestMapping(value = "loginSubmit.do")
    public ModelAndView loginSubmit(HttpServletRequest request, @Valid AdminUser user, Errors errors, ModelAndView modelAndView, RedirectAttributes model, RedirectView redirectView) throws Throwable {
        redirectView.setExposeModelAttributes(false);
        if (errors.hasErrors()) {
            redirectView.setUrl(htmlService.getBasePath(request) + "/admin/common/login.do");
            modelAndView.setView(redirectView);
            model.addFlashAttribute("errors", true);
            return modelAndView;
        }String pass ="";
//        String pass = new String(DigestUtil.bytes2hex(DigestUtil.MD5(user.getPassword())));
        AdminUserDto tmp = adminService.login(user.getUsername(), pass);
        if (null != tmp) {
            request.getSession().setAttribute(Constant.SESSION_USER, tmp);
            redirectView.setUrl(htmlService.getBasePath(request) + "/admin/common/index.do");
            modelAndView.setView(redirectView);
            return modelAndView;
        } else {
            model.addFlashAttribute("errors", true);
            redirectView.setUrl(htmlService.getBasePath(request) + "/admin/common/login.do");
            modelAndView.setView(redirectView);
            return modelAndView;
        }
    }

    @RequestMapping("logout.do")
    public ModelAndView logout(HttpServletRequest request, HttpSession session, ModelAndView modelAndView, RedirectView redirectView) throws Throwable {
        redirectView.setExposeModelAttributes(false);
        session.removeAttribute(Constant.SESSION_USER);
        redirectView.setUrl(htmlService.getBasePath(request) + "/admin/common/login.do");
        modelAndView.setView(redirectView);
        return modelAndView;
    }

    @RequestMapping(value = "index.do")
    public ModelAndView index(HttpServletRequest request, ModelAndView modelAndView) throws Throwable {
        modelAndView.setViewName("admin/common/index.htm");
        return modelAndView;
    }
}
