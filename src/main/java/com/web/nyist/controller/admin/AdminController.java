package com.web.nyist.controller.admin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.nyist.common.JsonReturnCode;
import com.web.nyist.dto.JSONError;
import com.web.nyist.dto.JSONResponse;
import com.web.nyist.dto.admin.DataGridPageDto;
import com.web.nyist.service.HtmlService;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by penghuiping on 1/16/15.
 */
public class AdminController {
    @Resource
    protected HtmlService htmlService;

    protected static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * form表单提交 Date类型数据绑定
     * <功能详细描述>
     *
     * @param binder
     * @see [类、类#方法、类#成员]
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * 用于处理异常的
     *
     * @return
     */
    @ExceptionHandler({Exception.class})
    public
    @ResponseBody
    JSONResponse exception(Exception e, HttpServletRequest request) {
        Logger.getLogger(AdminController.class).error(e.getMessage(), e);
        JSONResponse ret = new JSONResponse();
        ret.setCode(JsonReturnCode.服务器错误.value);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        ret.setMessage(sw.toString());
        ret.setErrors(new ArrayList<JSONError>());
        return ret;
    }

    protected <T> DataGridPageDto<T> toDataGridPageDto(Page<T> page) {
        DataGridPageDto<T> dataGridPageDto = new DataGridPageDto<T>();
        dataGridPageDto.setRows(page.getContent());
        dataGridPageDto.setTotal(page.getTotalElements());
        return dataGridPageDto;
    }

    protected<T> DataGridPageDto<T> tDataGridPageDto(List<T> content, Long size) {
        DataGridPageDto<T> dataGridPageDto = new DataGridPageDto<T>();
        dataGridPageDto.setRows(content);
        dataGridPageDto.setTotal(size);
        return dataGridPageDto;
    }

    protected JSONResponse succeed(Object obj) {
        JSONResponse ret = new JSONResponse();
        ret.setCode(JsonReturnCode.正常.value);
        ret.setReturnObject(obj);
        ret.setErrors(new ArrayList<JSONError>());
        return ret;
    }


    protected JSONResponse failed(ArrayList<JSONError> errors) {
        JSONResponse ret = new JSONResponse();
        ret.setCode(JsonReturnCode.业务逻辑错误.value);
        StringBuilder sb = new StringBuilder();
        if (null != errors) {
            for (JSONError error : errors) {
                sb.append(error.getElement() + ":" + error.getMessage()).append("<br>");
            }
        }
        ret.setMessage(sb.toString());
        ret.setErrors(errors);
        return ret;
    }

    protected JSONResponse failed(String message) {
        JSONResponse ret = new JSONResponse();
        ret.setCode(JsonReturnCode.业务逻辑错误.value);
        ret.setMessage(message);
        return ret;
    }

}
