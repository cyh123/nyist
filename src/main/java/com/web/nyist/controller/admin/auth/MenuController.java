package com.web.nyist.controller.admin.auth;

import com.google.common.collect.Lists;
import com.web.nyist.common.Constant;
import com.web.nyist.controller.admin.AdminController;
import com.web.nyist.dto.JSONResponse;
import com.web.nyist.dto.admin.AdminMenuButtonDto;
import com.web.nyist.dto.admin.AdminUserDto;
import com.web.nyist.model.AdminMenuButton;
import com.web.nyist.service.AdminService;
import com.web.nyist.service.db.AdminMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by penghuiping on 2/21/15.
 */
@Controller
@RequestMapping(MenuController.BASE_URL)
public class MenuController extends AdminController {
    public static final String BASE_URL = "/admin/auth/menu/";

    @Autowired
    AdminMenuService menuService;

    @Autowired
    AdminService adminService;

    @RequestMapping(value = "index.do")
    public ModelAndView index(ModelAndView modelAndView, HttpServletRequest request, Long id) throws Throwable {
        AdminUserDto user = (AdminUserDto) request.getSession().getAttribute(Constant.SESSION_USER);
        AdminMenuButtonDto adminMenuButtonDto = adminService.findOneMenuAndButtonById(user.getMenus(), id);
        modelAndView.addObject("buttons", adminMenuButtonDto.getChildren());
        modelAndView.setViewName(BASE_URL + "index.htm");
        return modelAndView;
    }

    @RequestMapping(value = "query.do")
    public
    @ResponseBody
    List<AdminMenuButtonDto> query() throws Throwable {
        List<AdminMenuButtonDto> menuDtos = adminService.findTreeMenus();
        return menuDtos;
    }


    @RequestMapping(value = "edit.do")
    public
    @ResponseBody
    AdminMenuButtonDto edit(Long id) throws Throwable {
        //edit
        AdminMenuButton menu = menuService.findOne(id);
        AdminMenuButtonDto menuDto = new AdminMenuButtonDto(menu);
        return menuDto;

    }

    @RequestMapping(value = "delete.do")
    public
    @ResponseBody
    JSONResponse delete(Long ids[]) throws Throwable {
        List<AdminMenuButton> menus = (List<AdminMenuButton>) menuService.findAll(Lists.newArrayList(ids));
        menuService.delete(menus);
        return succeed(true);
    }

    @RequestMapping(value = "editSubmit.do")
    public
    @ResponseBody
    JSONResponse editSubmit(@Valid AdminMenuButtonDto menu, Long parentId, Errors errors) throws Throwable {
        if (errors.hasErrors()) {
            return failed("出错啦！");
        }

        if (menu.getIsMenu()) {
            //菜单
            if (null != menu.getId() && menu.getId() > 0) {
                AdminMenuButton temp = menuService.findOne(menu.getId());
                temp.setName(menu.getName());
                temp.setUrl(menu.getUrl());
                temp.setEnable(menu.getEnable());
                menuService.save(temp);
            } else {
                if (null == parentId || 0 >= parentId) {
                    menu.setIsLeaf(true);
                    menuService.save(menu.toModel());
                } else {
                    AdminMenuButton parent = menuService.findOne(parentId);
                    parent.setIsLeaf(false);
                    menuService.save(parent);
                    menu.setParent(new AdminMenuButtonDto(parent));
                    menu.setIsLeaf(true);
                    menuService.save(menu.toModel());
                }

            }
        } else {
            //按钮
            if (null != menu.getId() && menu.getId() > 0) {
                AdminMenuButton temp = menuService.findOne(menu.getId());
                temp.setName(menu.getName());
                temp.setIcon(menu.getIcon());
                temp.setDescription(menu.getDescription());
                temp.setEnable(menu.getEnable());
                menuService.save(temp);
            } else {
                if (null == parentId || 0 >= parentId) {
                    menu.setIsLeaf(true);
                    menuService.save(menu.toModel());
                } else {
                    AdminMenuButton parent = menuService.findOne(parentId);
                    parent.setIsLeaf(false);
                    menuService.save(parent);
                    menu.setParent(new AdminMenuButtonDto(parent));
                    menu.setIsLeaf(true);
                    menuService.save(menu.toModel());
                }
            }
        }

        return succeed(true);
    }
}
