package com.web.nyist.controller.api;

import com.web.nyist.dto.JSONResponse;
import com.web.nyist.dto.admin.AdminUserDto;
import com.web.nyist.model.AdminUser;
import com.web.nyist.service.db.AdminUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by penghuiping on 1/6/15.
 */
@Controller
@RequestMapping("/api")
public class ApiController extends JSONController {
    private static final Logger logger = Logger.getLogger(ApiController.class);

    @Autowired
    AdminUserService adminUserService;

    @RequestMapping("/user.do")
    public
    @ResponseBody
    JSONResponse user(HttpServletRequest request) throws Throwable {
        List<AdminUser> users = adminUserService.findAll();
        List<AdminUserDto> adminUserDtos = AdminUserDto.from(users);
        return succeed(adminUserDtos);
    }

    @RequestMapping("/index.do")
    public String index(HttpServletRequest request) throws Throwable {
        request.setAttribute("a", "abc");
        return "index";
    }
}
