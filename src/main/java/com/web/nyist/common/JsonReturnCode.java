package com.web.nyist.common;

/**
 * Created by penghuiping on 9/24/15.
 */
public enum JsonReturnCode {
    业务逻辑错误(0),
    正常(1),
    正常未登入(2),
    服务器错误(3);

    public int value;

    JsonReturnCode(int value) {
        this.value = value;
    }

    public String getName() {
        return this.name();
    }
}
