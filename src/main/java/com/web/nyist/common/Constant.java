package com.web.nyist.common;

/**
 * Created by penghuiping on 1/5/15.
 */
public interface Constant {
    String SESSION_USER="USER";
    Integer PAGE_SIZE=5;
    Integer PAGE_FIRST=1;

    interface RANDOM {
        String ACCESS_TOKEN_PREFIX = "01";
        String IMAGE_ID_PREFIX = "02";
    }
}
