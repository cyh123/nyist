package com.web.nyist.common;

/**
 * Created by penghuiping on 16/4/20.
 */
public enum RoleLevel {
    超级管理员(0);

    public int value;

    RoleLevel(int value) {
        this.value = value;
    }

    public String getName() {
        return this.name();
    }
}
