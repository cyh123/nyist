package com.web.nyist.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Created by penghuiping on 16/4/4.
 */
@MappedSuperclass
public abstract class BaseSoftDeleteModel extends BaseModel {

    @Column
    private Integer enable;//0.无效 1.有效 2.已删除

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }
}
