package com.web.nyist.model;

import com.web.nyist.common.RoleLevel;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

/**
 * 后台管理操作实体类
 * Created by penghuiping on 1/13/15.
 */
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "t_admin_role")
public class AdminRole extends BaseSoftDeleteModel{
    @NotEmpty
    @Column(length = 45)
    private String name;

    @Enumerated
    @Column
    private RoleLevel level;//什么级别的管理员 此字段用于数据范围的限制

    @NotEmpty
    @Column
    private String description;

    @ManyToMany
    @JoinTable(
            name = "t_admin_role_admin_menu_button",
            joinColumns = {@JoinColumn(name = "admin_role_id")},
            inverseJoinColumns = {@JoinColumn(name = "admin_menu_button_id")}
    )
    private List<AdminMenuButton> adminMenuButtons;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleLevel getLevel() {
        return level;
    }

    public void setLevel(RoleLevel level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AdminMenuButton> getAdminMenuButtons() {
        return adminMenuButtons;
    }

    public void setAdminMenuButtons(List<AdminMenuButton> adminMenuButtons) {
        this.adminMenuButtons = adminMenuButtons;
    }
}
