package com.web.nyist.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.List;

/**
 * 后台管理菜单实体类
 * Created by penghuiping on 1/20/15.
 */
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "t_admin_menu_button")
public class AdminMenuButton extends BaseSoftDeleteModel{
    @Column(length = 45)
    private String name;

    @Column
    private String url;

    @OneToMany(mappedBy = "parent")
    private List<AdminMenuButton> children;

    @ManyToOne
    @JoinColumn(name = "parent")
    private AdminMenuButton parent;

    @Column(name = "is_leaf")
    private Boolean isLeaf;

    @Column
    private String icon;

    @Column(name = "is_menu")
    private Boolean isMenu;//判断是否是菜单，不是就是按钮

    @Column
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<AdminMenuButton> getChildren() {
        return children;
    }

    public void setChildren(List<AdminMenuButton> children) {
        this.children = children;
    }

    public AdminMenuButton getParent() {
        return parent;
    }

    public void setParent(AdminMenuButton parent) {
        this.parent = parent;
    }

    public Boolean getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Boolean isLeaf) {
        this.isLeaf = isLeaf;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(Boolean isMenu) {
        this.isMenu = isMenu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
