package com.web.nyist.task;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by penghuiping on 16/4/11.
 */
@Component
public class CommonTask {

    /**
     * 每两分钟打印一些东西
     */
    @Scheduled(cron = "0 0/2 * * * ? ")
    public void printSomething() {
        Logger.getLogger(CommonTask.class).info("hello something!");
    }
}
