package com.web.nyist.specification;

import com.web.nyist.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapperImpl;

import javax.persistence.criteria.*;
import java.util.Date;

/**
 * Created by penghuiping on 16/4/12.
 */
public class SearchParam {
    private String fieldName;
    private String value;
    private String operator;
    private final static BeanWrapperImpl convert = new BeanWrapperImpl();

    public SearchParam() {
    }

    public Predicate toPredicate(Root<?> root, CriteriaQuery<?> query,
                                 CriteriaBuilder builder) {
        Path expression = null;
        if (fieldName.contains(".")) {
            String[] names = StringUtils.split(fieldName, ".");
            expression = root.get(names[0]);
            for (int i = 1; i < names.length; i++) {
                expression = expression.get(names[i]);
            }
        } else {
            expression = root.get(fieldName);
        }

        Operator op = Operator.EQ;

        if (null != operator) {
            if ("eq".equals(operator.toLowerCase())) {
                op = Operator.EQ;
            } else if ("ne".equals(operator.toLowerCase())) {
                op = Operator.NE;
            } else if ("like".equals(operator.toLowerCase())) {
                op = Operator.LIKE;
            } else if ("gt".equals(operator.toLowerCase())) {
                op = Operator.GT;
            } else if ("lt".equals(operator.toLowerCase())) {
                op = Operator.LT;
            } else if ("gte".equals(operator.toLowerCase())) {
                op = Operator.GTE;
            } else if ("lte".equals(operator.toLowerCase())) {
                op = Operator.LTE;
            } else {
                op = Operator.EQ;
            }
        }

        Object objValue = null;
        if (expression.getJavaType().isInstance(new Date())) {
            if (value.trim().contains(":"))
                objValue = TimeUtil.parseDateTime(value);
            else
                objValue = TimeUtil.parseDate(value);
        } else {
            objValue = convert.convertIfNecessary(value, expression.getJavaType());
        }

        switch (op) {
            case EQ:
                return builder.equal(expression, objValue);
            case NE:
                return builder.notEqual(expression, objValue);
            case LIKE:
                return builder.like((Expression<String>) expression, "%" + objValue + "%");
            case LT:
                return builder.lessThan(expression, (Comparable) objValue);
            case GT:
                return builder.greaterThan(expression, (Comparable) objValue);
            case LTE:
                return builder.lessThanOrEqualTo(expression, (Comparable) objValue);
            case GTE:
                return builder.greaterThanOrEqualTo(expression, (Comparable) objValue);
            default:
                return null;
        }
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
