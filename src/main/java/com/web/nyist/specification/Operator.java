package com.web.nyist.specification;

/**
 * Created by penghuiping on 16/4/12.
 */
public enum Operator {
    EQ, NE, LIKE, GT, LT, GTE, LTE
}
