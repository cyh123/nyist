package com.web.nyist.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.web.nyist.model.AdminMenuButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penghuiping on 2016/2/23.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminMenuButtonDto {
    private Long id;

    private String name;

    private String url;

    private AdminMenuButtonDto parent;

    private Long parentId;

    private List<AdminMenuButtonDto> children;

    private Boolean isShow = false;

    private Boolean isLeaf;

    private Integer enable;

    private String icon;

    private Boolean isMenu;

    private String description;


    public AdminMenuButtonDto() {
    }

    public AdminMenuButtonDto(AdminMenuButton menu) {
        if (null == menu) {
            return;
        }
        this.id = menu.getId();
        this.name = menu.getName();
        this.url = menu.getUrl();
        this.isLeaf = menu.getIsLeaf();
        this.enable = menu.getEnable();
        this.icon = menu.getIcon();
        this.isMenu = menu.getIsMenu();
        this.description = menu.getDescription();
        this.children = new ArrayList<AdminMenuButtonDto>();

        if (null != menu.getParent()) {
            AdminMenuButton temp = menu.getParent();
            AdminMenuButtonDto parent = new AdminMenuButtonDto();
            parent.setId(temp.getId());
            parent.setIsLeaf(temp.getIsLeaf());
            parent.setName(temp.getName());
            parent.setUrl(temp.getUrl());
            parent.setIcon(temp.getIcon());
            parent.setIsMenu(temp.getIsMenu());
            parent.setDescription(temp.getDescription());
            this.parent = parent;
            this.parentId = parent.getId();
        }
    }

    public AdminMenuButtonDto(AdminMenuButton menu, List<AdminMenuButtonDto> children) {
        this(menu);
        if (null == menu) {
            return;
        }
        if (null == children)
            this.children = new ArrayList<AdminMenuButtonDto>();
        else
            this.children = children;
    }

    public static List<AdminMenuButtonDto> from(List<AdminMenuButton> menus) {
        List<AdminMenuButtonDto> menuDtos = new ArrayList<AdminMenuButtonDto>();
        for (AdminMenuButton menu : menus) {
            menuDtos.add(new AdminMenuButtonDto(menu));
        }
        return menuDtos;
    }

    public AdminMenuButton toModel() {
        AdminMenuButton adminMenuButton = new AdminMenuButton();
        adminMenuButton.setDescription(this.description);
        adminMenuButton.setEnable(this.enable);
        adminMenuButton.setIcon(this.icon);
        adminMenuButton.setName(this.name);
        adminMenuButton.setChildren(null);
        adminMenuButton.setIsLeaf(this.isLeaf);
        adminMenuButton.setIsMenu(this.isMenu);
        if (null != this.parent)
            adminMenuButton.setParent(this.parent.toModel());
        adminMenuButton.setUrl(this.url);
        adminMenuButton.setId(this.id);
        return adminMenuButton;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<AdminMenuButtonDto> getChildren() {
        return children;
    }

    public void setChildren(List<AdminMenuButtonDto> children) {
        this.children = children;
    }

    public AdminMenuButtonDto getParent() {
        return parent;
    }

    public void setParent(AdminMenuButtonDto parent) {
        this.parent = parent;
    }

    public Boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(Boolean isShow) {
        this.isShow = isShow;
    }

    public Boolean getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Boolean isLeaf) {
        this.isLeaf = isLeaf;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(Boolean isMenu) {
        this.isMenu = isMenu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
