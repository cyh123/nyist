package com.web.nyist.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.web.nyist.common.RoleLevel;
import com.web.nyist.model.AdminRole;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penghuiping on 2016/2/20.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminRoleDto {

    private Long id;

    private String name;

    private String description;

    private RoleLevel level;

    private Integer enable;

    private List<AdminMenuButtonDto> menus;

    public AdminRoleDto() {
    }

    public AdminRoleDto(AdminRole role) {
        this.id = role.getId();
        this.name = role.getName();
        this.description = role.getDescription();
        this.enable = role.getEnable();
        this.level = role.getLevel();
    }

    public AdminRoleDto(AdminRole role, List<AdminMenuButtonDto> menuDtos) {
        this(role);
        this.menus = menuDtos;
    }

    public static List<AdminRoleDto> from(List<AdminRole> roles) {
        List<AdminRoleDto> roleDtos = new ArrayList<AdminRoleDto>();
        for (AdminRole role : roles) {
            roleDtos.add(new AdminRoleDto(role));
        }
        return roleDtos;
    }

    public AdminRole toModel() {
        AdminRole adminRole = new AdminRole();
        adminRole.setId(this.id);
        adminRole.setName(this.name);
        adminRole.setDescription(this.description);
        adminRole.setLevel(this.level);
        adminRole.setEnable(this.enable);
        adminRole.setAdminMenuButtons(null);
        return adminRole;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public List<AdminMenuButtonDto> getMenus() {
        return menus;
    }

    public void setMenus(List<AdminMenuButtonDto> menus) {
        this.menus = menus;
    }

    public RoleLevel getLevel() {
        return level;
    }

    public void setLevel(RoleLevel level) {
        this.level = level;
    }

}
