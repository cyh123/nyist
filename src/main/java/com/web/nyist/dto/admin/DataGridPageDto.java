package com.web.nyist.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by penghuiping on 2016/2/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataGridPageDto<T> {
    private Long total;
    private List<T> rows;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
