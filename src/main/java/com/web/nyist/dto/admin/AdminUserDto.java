package com.web.nyist.dto.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.web.nyist.model.AdminUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by penghuiping on 16/3/31.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdminUserDto {
    private Long id;

    private String username;

    private String password;

    private String email;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date updateTime;

    private AdminRoleDto role;

    private List<AdminMenuButtonDto> menus;

    private Integer enable;

    public AdminUserDto() {
    }

    public AdminUserDto(AdminUser user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.createTime = user.getCreateTime();
        this.updateTime = user.getUpdateTime();
        this.enable = user.getEnable();
        this.role = new AdminRoleDto(user.getRole());
    }

    public AdminUserDto(AdminUser user, List<AdminMenuButtonDto> menuDtos) {
        this(user);
        this.menus = menuDtos;
    }

    public static List<AdminUserDto> from(List<AdminUser> users) {
        List<AdminUserDto> userDtos = new ArrayList<AdminUserDto>();
        for (AdminUser user : users) {
            userDtos.add(new AdminUserDto(user));
        }
        return userDtos;
    }

    public AdminUser toModel() {
        AdminUser adminUser = new AdminUser();
        adminUser.setUsername(this.username);
        adminUser.setId(this.id);
        adminUser.setCreateTime(this.createTime);
        adminUser.setEmail(this.email);
        adminUser.setPassword(this.password);
        adminUser.setRole(this.role.toModel());
        adminUser.setUpdateTime(this.updateTime);
        adminUser.setEnable(this.enable);
        return adminUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public AdminRoleDto getRole() {
        return role;
    }

    public void setRole(AdminRoleDto role) {
        this.role = role;
    }

    public List<AdminMenuButtonDto> getMenus() {
        return menus;
    }

    public void setMenus(List<AdminMenuButtonDto> menus) {
        this.menus = menus;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }
}
