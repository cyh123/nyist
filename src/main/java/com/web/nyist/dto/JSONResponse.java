package com.web.nyist.dto;

import java.util.ArrayList;

/**
 * 服务器返回对象
 * 所有服务器处理返回的统一对象
 */
public class JSONResponse {
    //0失败  1成功 2未登入
    private int code;
    //错误信息
    private ArrayList<JSONError> errors = null;
    //成功时返回的对象
    private Object returnObject = null;
    //提示信息
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setErrors(ArrayList<JSONError> errors) {
        this.errors = errors;
    }

    public ArrayList<JSONError> getErrors() {
        return errors;
    }

    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    public Object getReturnObject() {
        return returnObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
