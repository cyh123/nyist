package com.web.nyist;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by penghuiping on 16/8/26.
 */
public class RabbitMqTest extends BaseTest {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Override
    public void test() {
        rabbitTemplate.convertAndSend(AmqpConfig.EXCHANGE, AmqpConfig.ROUTINGKEY, "hello world");
    }
}
