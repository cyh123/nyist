package com.web.nyist;

import com.web.nyist.common.RoleLevel;
import com.web.nyist.model.AdminMenuButton;
import com.web.nyist.model.AdminRole;
import com.web.nyist.model.AdminUser;
import com.web.nyist.repository.AdminMenuButtonRepository;
import com.web.nyist.repository.AdminRoleRepository;
import com.web.nyist.repository.AdminUserRepository;
//import com.web.nyist.util.DigestUtil;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by penghuiping on 3/17/15.
 */
public class InitTest extends BaseTest {
    @Resource
    AdminMenuButtonRepository menuRepository;

    @Resource
    AdminRoleRepository roleRepository;

    @Resource
    AdminUserRepository userRepository;


    @Test
    public void initTest() throws Exception{
        /**角色配置菜单**/
        AdminMenuButton menu0 = new AdminMenuButton();
        menu0.setName("权限");
        menu0.setIsLeaf(false);
        menu0.setEnable(1);
        menu0.setIsMenu(true);

        AdminMenuButton menu0_0 = new AdminMenuButton();
        menu0_0.setName("用户管理");
        menu0_0.setParent(menu0);
        menu0_0.setUrl("/admin/auth/user/index.do");
        menu0_0.setIsLeaf(false);
        menu0_0.setEnable(1);
        menu0_0.setIsMenu(true);

        AdminMenuButton menu0_1 = new AdminMenuButton();
        menu0_1.setName("角色管理");
        menu0_1.setParent(menu0);
        menu0_1.setUrl("/admin/auth/role/index.do");
        menu0_1.setIsLeaf(false);
        menu0_1.setEnable(1);
        menu0_1.setIsMenu(true);

        AdminMenuButton menu0_2 = new AdminMenuButton();
        menu0_2.setName("菜单管理");
        menu0_2.setParent(menu0);
        menu0_2.setUrl("/admin/auth/menu/index.do");
        menu0_2.setIsLeaf(false);
        menu0_2.setEnable(1);
        menu0_2.setIsMenu(true);


        menuRepository.save(menu0);
        menuRepository.save(menu0_0);
        menuRepository.save(menu0_1);
        menuRepository.save(menu0_2);

        //按钮
        AdminMenuButton adminButton0 = new AdminMenuButton();
        adminButton0.setName("新增用户");
        adminButton0.setDescription("create");
        adminButton0.setIcon("icon-add");
        adminButton0.setEnable(1);
        adminButton0.setIsLeaf(true);
        adminButton0.setIsMenu(false);
        adminButton0.setParent(menu0_0);

        AdminMenuButton adminButton1 = new AdminMenuButton();
        adminButton1.setName("编辑用户");
        adminButton1.setDescription("edit");
        adminButton1.setIcon("icon-edit");
        adminButton1.setEnable(1);
        adminButton1.setIsLeaf(true);
        adminButton1.setIsMenu(false);
        adminButton1.setParent(menu0_0);

        AdminMenuButton adminButton2 = new AdminMenuButton();
        adminButton2.setName("删除用户");
        adminButton2.setDescription("delete");
        adminButton2.setIcon("icon-remove");
        adminButton2.setEnable(1);
        adminButton2.setIsLeaf(true);
        adminButton2.setIsMenu(false);
        adminButton2.setParent(menu0_0);

        AdminMenuButton adminButton3 = new AdminMenuButton();
        adminButton3.setName("新增角色");
        adminButton3.setDescription("create");
        adminButton3.setIcon("icon-add");
        adminButton3.setEnable(1);
        adminButton3.setIsLeaf(true);
        adminButton3.setIsMenu(false);
        adminButton3.setParent(menu0_1);

        AdminMenuButton adminButton4 = new AdminMenuButton();
        adminButton4.setName("编辑角色");
        adminButton4.setDescription("edit");
        adminButton4.setIcon("icon-edit");
        adminButton4.setEnable(1);
        adminButton4.setIsLeaf(true);
        adminButton4.setIsMenu(false);
        adminButton4.setParent(menu0_1);

        AdminMenuButton adminButton5 = new AdminMenuButton();
        adminButton5.setName("删除角色");
        adminButton5.setDescription("delete");
        adminButton5.setIcon("icon-remove");
        adminButton5.setEnable(1);
        adminButton5.setIsLeaf(true);
        adminButton5.setIsMenu(false);
        adminButton5.setParent(menu0_1);

        AdminMenuButton adminButton6 = new AdminMenuButton();
        adminButton6.setName("新增一级菜单");
        adminButton6.setDescription("createFirst");
        adminButton6.setIcon("icon-add");
        adminButton6.setEnable(1);
        adminButton6.setIsLeaf(true);
        adminButton6.setIsMenu(false);
        adminButton6.setParent(menu0_2);

        AdminMenuButton adminButton7 = new AdminMenuButton();
        adminButton7.setName("新增二级菜单");
        adminButton7.setDescription("createSecond");
        adminButton7.setIcon("icon-add");
        adminButton7.setEnable(1);
        adminButton7.setIsLeaf(true);
        adminButton7.setIsMenu(false);
        adminButton7.setParent(menu0_2);

        AdminMenuButton adminButton13 = new AdminMenuButton();
        adminButton13.setName("新增按钮");
        adminButton13.setDescription("createBtn");
        adminButton13.setIcon("icon-add");
        adminButton13.setEnable(1);
        adminButton13.setIsLeaf(true);
        adminButton13.setIsMenu(false);
        adminButton13.setParent(menu0_2);

        AdminMenuButton adminButton8 = new AdminMenuButton();
        adminButton8.setName("编辑");
        adminButton8.setDescription("edit");
        adminButton8.setIcon("icon-edit");
        adminButton8.setEnable(1);
        adminButton8.setIsLeaf(true);
        adminButton8.setIsMenu(false);
        adminButton8.setParent(menu0_2);

        AdminMenuButton adminButton9 = new AdminMenuButton();
        adminButton9.setName("删除");
        adminButton9.setDescription("delete");
        adminButton9.setIcon("icon-remove");
        adminButton9.setEnable(1);
        adminButton9.setIsLeaf(true);
        adminButton9.setIsMenu(false);
        adminButton9.setParent(menu0_2);

        menuRepository.save(adminButton0);
        menuRepository.save(adminButton1);
        menuRepository.save(adminButton2);
        menuRepository.save(adminButton3);
        menuRepository.save(adminButton4);
        menuRepository.save(adminButton5);
        menuRepository.save(adminButton6);
        menuRepository.save(adminButton7);
        menuRepository.save(adminButton13);
        menuRepository.save(adminButton8);
        menuRepository.save(adminButton9);

        List<AdminMenuButton> menus = new ArrayList<AdminMenuButton>();
        menus.add(menu0);
        menus.add(menu0_0);
        menus.add(menu0_1);
        menus.add(menu0_2);
        menus.add(adminButton0);
        menus.add(adminButton1);
        menus.add(adminButton2);
        menus.add(adminButton3);
        menus.add(adminButton4);
        menus.add(adminButton5);
        menus.add(adminButton6);
        menus.add(adminButton7);
        menus.add(adminButton8);
        menus.add(adminButton9);
        menus.add(adminButton13);

        //角色
        AdminRole role = new AdminRole();
        role.setEnable(1);
        role.setLevel(RoleLevel.超级管理员);
        role.setName("管理员");
        role.setDescription("管理员可以具有所有权限");
        role.setAdminMenuButtons(menus);
        roleRepository.save(role);

        //用户
        AdminUser user = new AdminUser();
        user.setUsername("admin");
        user.setEmail("admin@qq.com");
        user.setEnable(1);
        user.setCreateTime(new Date());
        String pass = "";
        user.setPassword(pass);
        user.setUpdateTime(new Date());
        user.setRole(role);

        userRepository.save(user);


    }
}
